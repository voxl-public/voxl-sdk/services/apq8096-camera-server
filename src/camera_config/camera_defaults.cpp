/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <string.h>
#include <ctype.h>

#include "camera_defaults.h"
#include "common_defs.h"
#define NUM_TOF_MODES 11

//todo: clean up this entire tof memory structure

//Supported modes (terminate with -1 for easier parseability)
static const int supportedTofModes[] = {5,9,-1};

//Supported framerates (terminate with -1 for easier parseability)
static const int tof5Framerates[] = {15, 30, 45, 60, -1};
static const int tof9Framerates[] = {5, 10, 15, 20, 30, -1};

const bool tofModeSupported(int mode)
{
    if(mode > NUM_TOF_MODES || mode <= 0) return false;
    for(int i = 0; supportedTofModes[i] != -1; i++){
        if(supportedTofModes[i] == mode) return true;
    }
    return false;
}
const bool tofFrameRateSupported(int mode, int framerate)
{

    if(!tofModeSupported(mode)) return false;

    const int *rates;

    switch (mode){
        case 5:
            rates = tof5Framerates;
            break;
        case 9:
            rates = tof9Framerates;
            break;
        default://shouldn't ever get here because tofModeSupported should eliminate other options
            return false;
    }

    for(int i = 0; rates[i] != -1; i++){
        if(rates[i] == framerate) return true;
    }

    return false;
}
const int* getSupportedTofModes()
{
    return supportedTofModes;
}
const int* getSupportedTofFramerates(int mode)
{

    if(!tofModeSupported(mode)) return NULL;

    switch (mode){
        case 5:
            return tof5Framerates;
            break;
        case 9:
            return tof9Framerates;
            break;
        default://shouldn't ever get here because tofModeSupported should eliminate other options
            return NULL;
    }
}

static const PerCameraInfo stereoDefaults = 
    {
        "stereo",                   //Name
        "",                         //Port (configure camera server fills this)
        CAMTYPE_STEREO,             //Type
        CAMAPI_HAL3,                //Access API
        30,                         //Framerate
        -1,                         //Tof mode
        -1,                         //Override ID (-1 ignores)
        true,                       //Enabled?
        {                           //Stream types info
            {                       //Preview Stream
                true,               //Enabled
                1280,               //Width
                480,                //Height
                PREVIEW_FMT_NV21    //Format
            },
            {                       //Video
                false,              //Enabled?
                0,                  //Width
                0,                  //Height
                VIDEO_FMT_INVALID   //Format
            },
            {                       //Snapshot
                false,              //Enabled?
                0,                  //Width
                0,                  //Height
                SNAPSHOT_FMT_INVALID//Format
            }
        },

        {                           //Exp/Gain Info
            -1,                     //Manual Exposure
            -1,                     //Manual Gain
            CAMAEALGO_MODALAI,        //AE Algorithm

            {                       //ModalAI AE Algorithm Parameters
                0,                  //Gain Min
                1000,               //Gain Max
                20,                 //Exposure Min
                33000,              //Exposure Max
                58.0,               //Desired MSV
                32000,              //k_p_ns
                20,                 //k_i_ns
                250,                //Max i
                3,                  //p Good Threshold
                1,                  //Exposure Period
                2,                  //Gain Period
                true,               //Display Debug
                8000,               //Exposure offset
            }
        }
    };

static const PerCameraInfo trackingDefaults = 
    {
        "tracking",                 //Name
        "",                         //Port (configure camera server fills this)
        CAMTYPE_TRACKING,           //Type
        CAMAPI_HAL3,                //Access API
        30,                         //Framerate
        -1,                         //Tof mode
        -1,                         //Override ID (-1 ignores)
        true,                       //Enabled?
        {                           //Stream types info
            {                       //Preview
                true,               //Enabled?
                640,                //Width
                480,                //Height
                PREVIEW_FMT_RAW8    //Format
            },
            {                       //Video
                false,              //Enabled?
                0,                  //Width
                0,                  //Height
                VIDEO_FMT_INVALID   //Format
            },
            {                       //Snapshot
                false,              //Enabled?
                0,                  //Width
                0,                  //Height
                SNAPSHOT_FMT_INVALID//Format
            }
        },

        {                           //Exp/Gain Info
            5259763,                //Manual Exposure
            1000,                   //Manual Gain
            CAMAEALGO_MODALAI,      //AE Algorithm

            {                       //ModalAI AE Algorithm Parameters
                100,                  //Gain Min
                1000,               //Gain Max
                20,                 //Exposure Min
                30000,              //Exposure Max
                68.0,               //Desired MSV
                6000.0,            //k_p_ns
                20.0,               //k_i_ns
                250.0,              //Max i
                3,                  //p Good Threshold
                2,                  //Exposure Period
                4,                  //Gain Period
                true,               //Display Debug
                3000,               //Exposure offset
            }
        }
    };

static const PerCameraInfo tofDefaults = 
    {
        "tof",                      //Name
        "",                         //Port (configure camera server fills this)
        CAMTYPE_TOF,                //Type
        CAMAPI_HAL3,                //Access API
        15,                         //Framerate
        9,                          //Tof mode
        -1,                         //Override ID (-1 ignores)
        true,                       //Enabled?
        {                           //Stream types info
            {                       //Preview Stream
                true,               //Enabled
                224,                //Width
                1557,               //Height
                PREVIEW_FMT_BLOB    //Format
            },
            {                       //Video
                false,              //Enabled?
                0,                  //Width
                0,                  //Height
                VIDEO_FMT_INVALID   //Format
            },
            {                       //Snapshot
                false,              //Enabled?
                0,                  //Width
                0,                  //Height
                SNAPSHOT_FMT_INVALID//Format
            }
        },

        {                           //Exp/Gain Info
            -1,                     //Manual Exposure
            -1,                     //Manual Gain
            CAMAEALGO_ISP,          //AE Algorithm

            {                       //ModalAI AE Algorithm Parameters
                0,                  //Gain Min
                0,                  //Gain Max
                0,                  //Exposure Min
                0,                  //Exposure Max
                0.0,                //Desired MSV
                0,                  //k_p_ns
                0,                  //k_i_ns
                0,                  //Max i
                0,                  //p Good Threshold
                0,                  //Exposure Period
                0,                  //Gain Period
                false,              //Display Debug
                0,                  //Exposure offset
            }
        }
    };

static const PerCameraInfo hiresDefaults = 
    {
        "hires",                    //Name
        "",                         //Port (configure camera server fills this)
        CAMTYPE_HIRES,              //Type
        CAMAPI_HAL3,                //Access API
        30,                         //Framerate
        -1,                         //Tof mode
        -1,                         //Override ID (-1 ignores)
        true,                       //Enabled?
        {                           //Stream types info
            {                       //Preview Stream
                true,               //Enabled
                640,                //Width
                480,                //Height
                PREVIEW_FMT_NV21    //Format
            },
            {                       //Video (disabled but available)
                false,              //Enabled?
                1024,               //Width
                768,                //Height
                VIDEO_FMT_H264      //Format
            },
            {                       //Snapshot (disabled but available)
                false,              //Enabled?
                1024,               //Width
                768,                //Height
                SNAPSHOT_FMT_JPG    //Format
            }
        },

        {                           //Exp/Gain Info
            -1,                     //Manual Exposure
            -1,                     //Manual Gain
            CAMAEALGO_ISP,          //AE Algorithm

            {                       //ModalAI AE Algorithm Parameters
                0,                  //Gain Min
                0,                  //Gain Max
                0,                  //Exposure Min
                0,                  //Exposure Max
                0.0,                //Desired MSV
                0,                  //k_p_ns
                0,                  //k_i_ns
                0,                  //Max i
                0,                  //p Good Threshold
                0,                  //Exposure Period
                0,                  //Gain Period
                false,              //Display Debug
                0,                  //Exposure offset
            }
        }
    };

static const PerCameraInfo emptyDefaults = 
    {
        "empty",                    //Name
        "",                         //Port (configure camera server fills this)
        CAMTYPE_INVALID,            //Type
        CAMAPI_INVALID,             //Access API
        0,                          //Framerate
        -1,                         //Tof mode
        -1,                         //Override ID (-1 ignores)
        false,                      //Enabled?
        {                           //Stream types info
            {                       //Preview Stream
                false,              //Enabled
                0,                  //Width
                0,                  //Height
                PREVIEW_FMT_INVALID //Format
            },
            {                       //Video (disabled but available)
                false,              //Enabled?
                0,                  //Width
                0,                  //Height
                VIDEO_FMT_INVALID   //Format
            },
            {                       //Snapshot (disabled but available)
                true,               //Enabled?
                0,                  //Width
                0,                  //Height
                SNAPSHOT_FMT_INVALID//Format
            }
        },

        {                           //Exp/Gain Info
            -1,                     //Manual Exposure
            -1,                     //Manual Gain
            CAMAEALGO_INVALID,      //AE Algorithm

            {                       //ModalAI AE Algorithm Parameters
                0,                  //Gain Min
                0,                  //Gain Max
                0,                  //Exposure Min
                0,                  //Exposure Max
                0.0,                //Desired MSV
                0,                  //k_p_ns
                0,                  //k_i_ns
                0,                  //Max i
                0,                  //p Good Threshold
                0,                  //Exposure Period
                0,                  //Gain Period
                false,              //Display Debug
                0,                  //Exposure offset
            }
        }
    };

const PerCameraInfo getDefaultCameraInfo(CameraType t) {
    switch(t){
        case CAMTYPE_STEREO:
            return stereoDefaults;
        case CAMTYPE_TRACKING:
            return trackingDefaults;
        case CAMTYPE_TOF:
            return tofDefaults;
        case CAMTYPE_HIRES:
            return hiresDefaults;
        default:
            return emptyDefaults;
    }
}