/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "common_defs.h"
#include "debug_log.h"
#include "expgain_interface_modalai.h"



// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// -----------------------------------------------------------------------------------------------------------------------------
Status ExpGainModalAI::Initialize(const ExpGainInterfaceData* pIntfData)
{

    exposure = new ModalExposureHist(*((modal_exposure_config_t*)(pIntfData->pAlgoSpecificData)));

    Status status = S_OK;
    VOXL_LOG_ALL( "%s\n", __FUNCTION__ );
    if (pIntfData != NULL)
    {
        VOXL_LOG_ALL( "%s\n", __FUNCTION__ );

        width = pIntfData->width;
        height = pIntfData->height;

        // TODO: Add parameter parsing
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function performs any clean up tasks prior to object destruction
// -----------------------------------------------------------------------------------------------------------------------------
void ExpGainModalAI::Destroy()
{
    if(exposure != NULL){
        delete exposure;
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Adds a frame to be used for calculating new values of exposure/gain
// -----------------------------------------------------------------------------------------------------------------------------
Status ExpGainModalAI::GetNewExpGain(const ExpGainFrameData* pNewFrame, ExpGainResult* pNewExpGain)
{
    validValues = false;
    int64_t set_exposure_ns;
    int32_t  set_gain;
    static int frame_count = 0;

    if( pNewFrame && pNewFrame->pFramePixels )
    { 
        // TODO: use the real exposures and gains
        /*if( frame_count > 0 )
        {
            currentExposureNs = pNewExpGain->exposure;
            currentGain = pNewExpGain->gain;    
        }
        else
        {
            currentExposureNs = 10000;
            currentGain = 512;
        }*/
        
        validValues = exposure->update_exposure(
            (uint8_t*)(pNewFrame->pFramePixels),
            width,
            height,
            currentExposureNs,
            currentGain,
            &set_exposure_ns,
            &set_gain
        );
    }

    if( validValues )
    {
        pNewExpGain->exposure = set_exposure_ns;
        pNewExpGain->gain     = set_gain;

        currentExposureNs = set_exposure_ns;
        currentGain     = set_gain;

        VOXL_LOG_ALL( "Internal AE new val:exposure %llu gain %d\n",
            set_exposure_ns, set_gain );
    }
    else
    {
        pNewExpGain->exposure = currentExposureNs;
        pNewExpGain->gain     = currentGain;
    }

    frame_count++;
    return S_OK;
}